import re

from rest_framework import serializers


class SearchSerializer(serializers.Serializer):
    checkin = serializers.CharField(max_length=100, allow_blank=True)
    checkout = serializers.CharField(max_length=100, allow_blank=True)
    destination = serializers.CharField(max_length=100, allow_blank=True)
    guests = serializers.CharField(max_length=100, allow_blank=True)
    suppliers = serializers.RegexField(max_length=100, allow_blank=True, regex=re.compile(r'^\w+(,\w+)*$'))

import requests

from django.core.cache import cache

from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import SearchSerializer

SUPPLIER_DICT = {
    'supplier1': 'https://api.myjson.com/bins/2tlb8',
    'supplier2': 'https://api.myjson.com/bins/42lok',
    'supplier3': 'https://api.myjson.com/bins/15ktg'
}


class SearchView(APIView):
    serializer_class = SearchSerializer

    @staticmethod
    def format_results(results):
        return [{'id': k, 'price': v['price'], 'supplier': v['supplier']} for k, v in results.items()]

    def get(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        cache_key = hash(frozenset(serializer.validated_data.items()))
        cache_value = cache.get(cache_key)

        if cache_value:
            return Response(cache_value, status=status.HTTP_200_OK)

        suppliers = request.data['suppliers']

        if suppliers:
            suppliers = suppliers.split(',')
            for item in suppliers:
                if item not in SUPPLIER_DICT:
                    raise ValidationError('Unknown supplier: {}'.format(item))
        else:
            suppliers = SUPPLIER_DICT.keys()

        # I expect that storing the results in the memory should not cause any significant memory load
        # If so, some permanent storage should be used for the result aggregation
        results = {}

        for supplier in suppliers:
            supplier_cache_key = hash(supplier)
            supplier_cache_value = cache.get(supplier_cache_key)

            if supplier_cache_value:
                r = supplier_cache_value
            else:
                r = requests.get(SUPPLIER_DICT[supplier])
                cache.set(supplier_cache_key, r)

            for id, price in r.json().items():
                if not results.get(id) or results[id]['price'] > price:
                    results[id] = {'price': price, 'supplier': supplier}

        formatted_results = self.format_results(results)
        cache.set(cache_key, formatted_results)

        return Response(formatted_results, status=status.HTTP_200_OK)

    def post(self, request):
        return self.get(request)

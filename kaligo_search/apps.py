from django.apps import AppConfig


class KaligoSearchConfig(AppConfig):
    name = 'kaligo_search'
